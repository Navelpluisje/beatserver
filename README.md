# BeatServer

A simple DDP-server for working with the [BeatMachine](https://bitbucket.org/Navelpluisje/beatmachine). This will make it possible to work with multiple persons on the BeatMachine and create a musical paradise.

## running

- `npm install`
- `node beatserver`

## Options

This script will only accept a numeric parameter. This will be the port on which the server will be running. It defaults to `3000` The command will then look like: `node beatserver 3030` to run on port **3030**

