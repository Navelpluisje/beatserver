const DDPServer = require('ddp-server-reactive');
const exec = require('child_process').exec;
const args = process.argv.slice(2);

// Create a server listening on the default port 3000
const server = new DDPServer({ port: process.env.PORT || 3000 });

// Create a reactive collection
// All the changes below will automatically be sent to subscribers
const stepvalues = server.publish('stepvalues');

// Add items
const sounds = [
    'kick',
    'snare',
    'rim',
    'hat',
    'hat2',
    'crash',
    'ride',
    'clap'
];
const steps = 64;

sounds.forEach((channel) => {
    stepvalues[channel] = [];
    Array.from(Array(steps)).forEach((x, index) => {
        stepvalues[channel][index] = {
            channel,
            step: index,
            value: false,
        };
    });
});

// Add methods
server.methods({
    click: function (channel, step, value) {
        stepvalues[channel][step] = {
            channel,
            step,
            value,
        };
        return true;
    }
});

console.log(`started on port ${process.env.PORT || 3000}`);
